<?php

require_once '../src/controllers/CityController.php';
require_once '../src/controllers/CountryController.php';
require_once '../src/controllers/UserController.php';
require_once '../src/controllers/DefaultController.php';
require_once '../src/utils/SingletonDataBase.php';
require_once '../src/utils/SingletonConfigReader.php';
require_once '../src/utils/Renderer.php';
require_once '../src/utils/Paginator.php';


if (isset($_SERVER["PATH_INFO"])) {
    $path = trim($_SERVER["PATH_INFO"], "/");
} else {
    $path = "";
}

$fragments = explode("/", $path);

$control = array_shift($fragments);


/*
 * gestion des routes
 */
switch ($control) {
    /*
     * Gestion des routes pour city avec la méthode GET et POST
     */
    case "city" : {
            if ($_SERVER["REQUEST_METHOD"] == "GET") {
                cityRoutes_get($fragments);
            }
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                cityRoutes_post($fragments);
            }
            break;
        }
    /*
     * Gestion des routes pour city avec la méthode GET et POST
     */
    case "country" : {
            //echo "Gestion des routes pour country<hr>";
            if ($_SERVER["REQUEST_METHOD"] == "GET") {
                countryRoutes_get($fragments);
            }
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                countryRoutes_post($fragments);
            }
            break;
        }
    /*
     * Gestion des routes pour user avec la méthode GET et POST
     */
    case "user" : {
            //echo "Gestion des routes pour user <hr>";
            //calling function to prevend all hard code here
            if ($_SERVER["REQUEST_METHOD"] == "GET") {
                userRoutes_get($fragments);
            }
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                userRoutes_post($fragments);
            }
            break;
        }
    default : {
            call_user_func_array([new DefaultController(), "show_continent"], $fragments);
        }
}


/*
 * Routes de city avec la méthode GET qui ajoute les données à l'url 
 */

function cityRoutes_get($fragments) {

    $action = array_shift($fragments);

    switch ($action) {
        case "show" : {
                call_user_func_array([new CityController(), "display"], $fragments);
                break;
            }
        case "show_city" : {
                call_user_func_array([new CityController(), "show_city"], $fragments);
                break;
            }
        case "edit_city" : {
                call_user_func_array([new CityController(), "edit_city"], $fragments);
                break;
            }
        case "show_add_city" : {
                call_user_func_array([new CityController(), "show_add_city"], $fragments);
                break;
            }
        case "delete_city" : {
                call_user_func_array([new CityController(), "delete_city"], $fragments);
                break;
            }
        case "show_delete_city" : {
                call_user_func_array([new CityController(), "show_add_city"], $fragments);
                break;
            }
        default : {
                call_user_func_array([new DefaultController(), "show_continent"], $fragments);
                //Gestion du probleme
            }
    }
}

/*
 * Routes de city avec la méthode POST qui permet de garder de la confidentialité & de la flexibilité
 */

function cityRoutes_post($fragments) {

    $action = array_shift($fragments);

    switch ($action) {
        case "update_city": {
                call_user_func_array([new CityController(), "update_city"], $fragments);
                break;
            }
        case "add_city" : {
                call_user_func_array([new CityController(), "add_city"], $fragments);
                break;
            }
        default: {
                call_user_func_array([new DefaultController(), "show_continent"], $fragments);
                break;
            }
    }
}

/*
 * Routes de country avec la méthode GET qui ajoute les données à l'url 
 */

function countryRoutes_get($fragments) {

    $action = array_shift($fragments);

    switch ($action) {
        case "show_country" : {
                call_user_func_array([new CountryController(), "show_country"], $fragments);
                break;
            }
        case "show_city" : {
                call_user_func_array([new CountryController(), "show_city"], $fragments);
                break;
            }
        case "show_add_country" : {
                call_user_func_array([new CountryController(), "show_add_country"], $fragments);
                break;
            }
        case "edit_country" : {
                call_user_func_array([new CountryController(), "edit_country"], $fragments);
                break;
            }
        case "delete_country" : {
                call_user_func_array([new CountryController(), "delete_country"], $fragments);
                break;
            }
        default : {
                call_user_func_array([new DefaultController(), "show_continent"], $fragments);
                //Gestion du probleme
            }
    }
}

/*
 * Routes de Country avec la méthode POST qui permet de garder de la confidentialité & de la flexibilité
 */

function countryRoutes_post($fragments) {

    $action = array_shift($fragments);

    switch ($action) {
        case "update_country": {
                call_user_func_array([new CountryController(), "update_country"], $fragments);
                break;
            }
        case "add_country" : {
                call_user_func_array([new CountryController(), "add_country"], $fragments);
                break;
            }
        case "show_city" : {
                call_user_func_array([new CountryController(), "show_city"], $fragments);
                break;
            }
        default: {
                call_user_func_array([new DefaultController(), "show_continent"], $fragments);
                break;
            }
    }
}

/*
 * Routes de user avec la méthode GET qui ajoute les données à l'url 
 */

function userRoutes_get($fragments) {

    $action = array_shift($fragments);

    switch ($action) {
        case "show_connexion_user" : {
                call_user_func_array([new UserController(), "show_connexion_user"], $fragments);
                break;
            }
        case "show_edit_user" : {
                call_user_func_array([new UserController(), "show_edit_user"], $fragments);
                break;
            }
        case "check_deconnexion" : {
                call_user_func_array([new UserController(), "check_deconnexion"], $fragments);
                break;
            }
        case "show_add_user" : {
                call_user_func_array([new UserController(), "show_add_user"], $fragments);
                break;
            }
        default : {
                call_user_func_array([new DefaultController(), "show_continent"], $fragments);
                //Gestion du probleme
            }
    }
}

/*
 * Routes de user avec la méthode POST qui permet de garder de la confidentialité & de la flexibilité
 */

function userRoutes_post($fragments) {

    $action = array_shift($fragments);

    switch ($action) {
        case "show_edit_user": {
                call_user_func_array([new UserController(), "show_edit_user"], $fragments);
                break;
            }

        case "check_connexion": {
                call_user_func_array([new UserController(), "check_connexion"], $fragments);
                break;
            }

        case "check_inscription" : {
                call_user_func_array([new UserController(), "check_inscription"], $fragments);
                break;
            }

        default: {
                call_user_func_array([new DefaultController(), "show_continent"], $fragments);
                break;
            }
    }
}
