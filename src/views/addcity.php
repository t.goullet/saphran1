<!DOCTYPE html>
<html lang="en">
    
    <!-- formulaire d'ajout d'une ville dans un pays
        grace a la récupération du code du pays choisi.
        Tous les champs du formulaire sont "required" car aucune information
        ne doit etre manquante dans une ville.
    -->
    
    
    <head>
        <title>Add City</title>
        <?php include('head.php'); ?>
    </head>
    <body>
        <?php include('nav.php'); ?>
        <br>
        <br>
        <form id="login-form" class="form" method="POST" action="/city/add_city">
            <h3 class="text-center text" name="txt">Ajouter une ville pour le pays <?php echo $_GET['CountryCode'] ?></h3>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 offset-sm-3 mt-5">
                       
                        <div class="input-group mb-3">
                            <div class="input-group-prepend col-2">
                                <span class="input-group-text">Id : </span>
                            </div>
                            <input type="" class="form-control" name="Id" value="Un id va vous être attribué automatiquement" readonly="">
                        </div>   
                        
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Pays : </span>
                            </div>
                            <input type="text" class="form-control" name="CountryCode" value="<?php echo $_GET['CountryCode'] ?>" readonly="">
                        </div>  
                        
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Nom : </span>
                            </div>
                            <input type="text" class="form-control" name="Name" value="" required="">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">District : </span>
                            </div>
                            <input type="text" class="form-control" name="District" value="" required="">
                        </div>  

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Population : </span>
                            </div>
                            <input type="number" class="form-control" name="Population" value="" required="">
                        </div>
                        
                        <input type="hidden" value="<?= $csrf_token; ?>" name="csrf_token" id="csrf_token" required="">  
                        
                          <div class="row">
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-primary" name="btnadd">Ajouter</button>
                                
                                <!--le bouton reset permet dee revenir a l'etat initial du formulaire -->
                                
                                <button type="reset" class="btn btn-success" name="btnreset">reset</button>
                            </div>
                        </div>

                        
                    </div>

                </div>

            </div>
        </form>
    </body>
</html>