<!DOCTYPE html>
<html lang="en">
    
    <!-- formulaire d'ajout d'un pays dans un continent.
        Certains champs ne sont pas required car dans la base de données
    il existe des pays ou des informations sont manquantes comme l'année
    d'indépendance.
    Une fois ajouté, le pays se retrouve affiché dans la table de son continent.
    -->
    
    <head>
        <title>Add Pays</title>
        <?php include('head.php'); ?>
    </head>
    
    <body>
        <?php include('nav.php'); ?>
        <br>
        <br>
        <form id="login-form" class="form" method="POST" action="/country/add_country">
            <h3 class="text-center text" name="txt">Ajouter un Pays</h3>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 offset-sm-3 mt-5">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend col-2">
                                <span class="input-group-text">id : </span>
                            </div>
                            <input type="" class="form-control" name="id" value="Un id va vous être attribué automatiquement" readonly="">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Nom : </span>
                            </div>
                            <input type="text" class="form-control" name="nom" value="" required="">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Code : </span>
                            </div>
                            <input type="text" pattern="[A-Z]{3}" class="form-control" name="code" value="" required="">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Continent : </span>
                            </div>
                            <input type="text" class="form-control" name="continent" value="<?php echo $_GET['continent'] ?>" required="" readonly="">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Region : </span>
                            </div>
                            <input type="text" class="form-control" name="region" value="" required="">
                        </div>
                        
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Population : </span>
                            </div>
                            <input type="number" class="form-control" name="population" value="" required="">
                        </div>
                        
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Capitale : </span>
                            </div>
                            <input type="number" class="form-control" name="capital" value="" required="">
                        </div>
                        
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Surface Area : </span>
                            </div>
                            <input type="number" class="form-control" name="surfaceArea" value="" required="">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Independance Year : </span>
                            </div>
                            <input type="number" pattern="" class="form-control" name="indepYear" value="">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Life Expectancy : </span>
                            </div>
                            <input type="number" class="form-control" name="lifeExpectancy" value="">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">GNP : </span>
                            </div>
                            <input type="number" class="form-control" name="gnp" value="" required="">
                        </div> 
                        
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">GNPOld : </span>
                            </div>
                            <input type="number" class="form-control" name="gnpOld" value="">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Local Name : </span>
                            </div>
                            <input type="text" class="form-control" name="localName" value="" required="">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Government Form : </span>
                            </div>
                            <input type="text" class="form-control" name="governmentForm" value="" required="">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Head of State : </span>
                            </div>
                            <input type="text" class="form-control" name="headOfState" value="" required="">
                        </div> 
                        
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Code2 : </span>
                            </div>
                            <input type="text" pattern="[A-Z]{2}" class="form-control" name="code2" value="" required="">
                        </div>
                        
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Image1 : </span>
                            </div>
                            <input type="text" class="form-control" name="image1" value="">
                        </div>
                        
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Image2 : </span>
                            </div>
                            <input type="text" class="form-control" name="image2" value="">
                        </div>
                        
                        <input type="hidden" value="<?= $csrf_token; ?>" name="csrf_token" id="csrf_token" required="">  

                        <div class="row">
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-primary" name="btnadd">Ajouter</button>
                                <button type="reset" class="btn btn-success" name="btnreset">reset</button>
                            </div>
                        </div>
                        
                    </div>

                </div>

            </div>
        </form>
    </body>
</html>