<!DOCTYPE html>
<html lang="en">
    <head>
        
        <!-- formulaire de modification d'une ville ou l'on récupere les données de la ville choisie -->
        
        <title>Edit Pays</title>
        <?php include('head.php'); ?>
    </head>
    <body>
        <?php include('nav.php'); ?>
        <br>
        <br>
        <form method="POST" action="/city/update_city/<?php echo $city->getCity_Id();?>">
            <h3 class="text-center text" name="txt">Modifier la ville de <?php echo $city->getName();?></h3>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 offset-sm-3 mt-5">

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Id : </span>
                            </div>
                            <input type="text" class="form-control" name="id" value="<?php echo $city->getCity_Id();?>" readonly="">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Nom : </span>
                            </div>
                            <input type="text" class="form-control" name="Name" value="<?php echo $city->getName();?>" required="">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">District : </span>
                            </div>
                            <input type="text" class="form-control" name="District" value="<?php echo $city->getDistrict();?>" required="">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Population : </span>
                            </div>
                            <input type="text" class="form-control" name="Population" value="<?php echo $city->getPopulation();?>" required="">
                        </div>   

                       <input type="hidden" value="<?= $csrf_token; ?>" name="csrf_token" id="csrf_token" required="">  

                        <div class="row">
                            <div class="col-sm-6">
                                 <button type="submit" class="btn btn-primary" name="btnupdate">Valider</button>
                                <button type="reset" class="btn btn-success" name="btnreset">reset</button>
                            </div>

                        </div>
                        
                    </div>

                </div>

            </div>
        </form>
    </body>
</html>
