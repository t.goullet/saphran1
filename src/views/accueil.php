<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Accueil</title>
        <?php include('head.php'); ?>
    </head>
    
    <body>
        <?php include('nav.php'); ?>

    <br>
        <div class="container p-3 my-3 px-5">
            <div class="card-deck">
                <?php
                /*
                 * pour chaque continent présent dans la base de données,
                 * une card sera générée avec :
                 * -le nom du continent
                 * -une image aléatoire associée au nom du continent grace
                 * a Loremflickr.com
                 */
                    foreach ($continent as $value) {                    
                ?>
                <div class="card mb-5" style="width:300px;flex: inherit;">
                    <img class="card-img-top" src="http://loremflickr.com/400/300/<?php echo $value['continent'];?>" alt="Card image" style="width:100%">
                    <div class="card-body " style="text-align: center">
                        <a href="/country/show_country/<?php echo $value['continent'];?>?page=1"><?php echo $value['continent'] ?></a>
                        <p class="card-text" ">Découvrez tous les pays de <?php echo $value['continent'] ?></p>
                    </div>
                </div>
                <?php
                    }
                ?>
            </div>
        </div>
    </body>
</html>