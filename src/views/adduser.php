<?php
/*on importe le fakerer pour avoir des données déja présente dans le formulaire*/
require_once '../src/utils/fakerer.php';

?>

<!DOCTYPE html>
<html lang="en">
    
    <!-- formulaire d'ajout d'un utilisateur. Il s'inscrit dans la base de données.
        Tous les champs du formulaire sont "required" car aucune information
        ne doit etre manquante a un utilsateur.
        Son mot de passe est hashé dans la base de données. -->
    
    <head>
        <title>Add User</title>
        <?php include('head.php'); ?>
    </head>
    
    <body>
        <?php include('nav.php'); ?>
        <br>
        <br>
        <form id="login-form" class="form" method="POST" action="/user/check_inscription">
            <h3 class="text-center text" name="txt">Ajout d'un Utilisateur</h3>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 offset-sm-3 mt-5">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend col-2">
                                <span class="input-group-text">id : </span>
                            </div>
                            <input type="" class="form-control" name="Id" value="Un id va vous être attribué automatiquement" readonly="">
                        </div>   
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Mail : </span>
                            </div>
                            <input type="text" class="form-control" name="Mail" value="<?php echo $fakerPrenom ?>.<?php echo $fakerNom ?>@<?php echo $fakerEmailDomain ?>" required="" >
                        </div>  
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Nom : </span>
                            </div>
                            <input type="text" class="form-control" name="Nom" value="<?php echo $fakerNom ?>" required="">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Prenom : </span>
                            </div>
                            <input type="text" class="form-control" name="Prenom" value="<?php echo $fakerPrenom ?>" required="">
                        </div>  

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Password : </span>
                            </div>
                            <input type="text" class="form-control" name="Password" value="<?php echo $fakerPassword ?>" required="">
                        </div>
                        
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Role : </span>
                            </div>
                            <select type="number" class="form-control" name="User_Roles_Id" value="" required="">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                            </select>
                        </div>
                        
                        <input type="hidden" value="<?= $csrf_token; ?>" name="csrf_token" id="csrf_token" required="">  
                        
                        <div class="row">
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-primary" name="btnadd">Ajouter</button>
                                <button type="reset" class="btn btn-success" name="btnreset">Reset</button>
                            </div>
                        </div>

                        
                    </div>

                </div>

            </div>
        </form>
    </body>
</html>