<!DOCTYPE html>
<html>
    <!-- formulaire de connexion de l'utilisateur. -->
    <head>
        <?php include('head.php'); ?>
        <title>Connexion User</title>
    </head>
    
    <body>
        <?php include('nav.php'); ?>
        <br>
        <br>
        <div id="login">
            <br>
            <div class="container">
                <div id="login-row" class="row justify-content-center align-items-center">
                    <div id="login-column" class="col-md-6">
                        <div id="login-box" class="col-md-12">
                            
                            
                            <form id="login-form" class="form" method="POST" action="/user/check_connexion">
                                <h3 class="text-center text" name="txt">Se connecter :</h3>
                                <div class="form-group">
                                    <label for="email" class="text" name="txt">Adresse Mail :</label><br>
                                    <input type="email" name="Mail" id="Mail" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="password" class="text" name="txt">Mot de passe:</label><br>
                                    <input type="password" name="Password" id="Password" class="form-control">
                                </div>
                                
                                <div class="form-group">
                                    <input class="btn btn-info" name="btnconnexion" type="submit" value="Connexion">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>