<!DOCTYPE html>
<html lang="en">
    
    <!-- formulaire de modification d'un pays ou l'on récupere les données du pays choisi -->
    
    <head>
        <title>Edit Pays</title>
        <?php include('head.php'); ?>
    </head>
    <body>
        <?php include('nav.php'); ?>
        <br>
        <form method="POST" action="/country/update_country/<?php echo $country->getName() ?>">
            <h3 class="text-center text" name="txt">Modifier un Pays</h3>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 offset-sm-3 mt-5">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend col-2">
                                <span class="input-group-text">id : </span>
                            </div>
                            <input type="number" class="form-control" name="id" value="<?php echo $country->getCountry_Id() ?>" readonly="">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Nom : </span>
                            </div>
                            <input type="text" class="form-control" name="nom" value="<?php echo $country->getName() ?>" required="">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Code : </span>
                            </div>
                            <input type="text" pattern="[A-Z]{3}" class="form-control" name="code" value="<?php echo $country->getCode() ?>" required="">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Continent : </span>
                            </div>
                            <input type="text" class="form-control" name="continent" value="<?php echo $country->getContinent() ?>" required="" readonly="">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Region : </span>
                            </div>
                            <input type="text" class="form-control" name="region" value="<?php echo $country->getRegion() ?>" required="">
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Population : </span>
                            </div>
                            <input type="number" class="form-control" name="population" value="<?php echo $country->getPopulation() ?>" required="">
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Capitale : </span>
                            </div>
                            <input type="number" class="form-control" name="capital" value="<?php echo $country->getCapital() ?>" required="">
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Surface Area : </span>
                            </div>
                            <input type="number" class="form-control" name="surfaceArea" value="<?php echo $country->getSurfaceArea() ?>" required="">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Independance Year : </span>
                            </div>
                            <input type="number" pattern="" class="form-control" name="indepYear" value="<?php echo $country->getIndepYear() ?>">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Life Expectancy : </span>
                            </div>
                            <input type="number" class="form-control" name="lifeExpectancy" value="<?php echo $country->getLifeExpectancy() ?>">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">GNP : </span>
                            </div>
                            <input type="number" class="form-control" name="gnp" value="<?php echo $country->getGNP() ?>" required="">
                        </div> 

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">GNPOld : </span>
                            </div>
                            <input type="number" class="form-control" name="gnpOld" value="<?php echo $country->getGNPOld() ?>">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Local Name : </span>
                            </div>
                            <input type="text" class="form-control" name="localName" value="<?php echo $country->getLocalName() ?>" required="">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Government Form : </span>
                            </div>
                            <input type="text" class="form-control" name="governmentForm" value="<?php echo $country->getGovernmentForm() ?>" required="">
                        </div>   

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Head of State : </span>
                            </div>
                            <input type="text" class="form-control" name="headOfState" value="<?php echo $country->getHeadOfState() ?>" required="">
                        </div> 

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Code2 : </span>
                            </div>
                            <input type="text" class="form-control" name="code2" value="<?php echo $country->getCode2() ?>">
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Image1 : </span>
                            </div>
                            <input type="text" class="form-control" name="image1" value="<?php echo $country->getImage1() ?>">
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Image2 : </span>
                            </div>
                            <input type="text" class="form-control" name="image2" value="<?php echo $country->getImage2() ?>">
                        </div>

                        <input type="hidden" value="<?= $csrf_token; ?>" name="csrf_token" id="csrf_token" required="">

                        <div class="row">
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-primary" name="btnupdate">Valider</button>
                                <button type="reset" class="btn btn-success" name="btnreset">reset</button>
                            </div>
                            <div class="col-sm-6 text-right">
                                <a class="btn btn-large btn-dark" href="/country/show_country/<?php echo $country->getContinent() ?>">back</a>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </form>
    </body>
</html>