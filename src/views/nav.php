<!-- code comportant toute la vue de la barre de navigation -->

<?php
if (session_id() == "") {
    session_start();
}
?>
<!DOCTYPE html>
<html lang="en">
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="">

                <img src="http://localhost:8000/img/world.jpg" width="50" height="50" alt="" loading="lazy">
                <a class="navbar-brand">WorldDB</a>

                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li class="nav-item active">
                        <a class="nav-link" href="http://localhost:8000/index.php">Accueil</a>
                    </li>

                    <!--En fonction du statut de la session (active ou non), la barre
                    de navigation change d'apparence.
                    Lorsque l'utilisateur n'est pas connecté il peut s'inscrire ou se connecter.
                    -->

                    <?php if (!isset($_SESSION['User'])) { ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/user/show_connexion_user">Connexion</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/user/show_add_user">Inscription</a>
                        </li>

                        <!--
                        Lorsque l'utilisateur est connecté il peut se déconnecter.
                        -->

                        <?php
                    } else {
                        ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/user/check_deconnexion">Deconnexion</a>
                        </li>

                        <li class="nav-item">



                        </li>

                    </ul>
                    <form class="form-inline my-2 my-lg-0">

                        <button class="btn btn-outline-primary my-2 my-sm-0" type="submit"><?php
                            echo "Bienvenue ", $_SESSION['User']->getNom(), " ", $_SESSION['User']->getPrenom();
                        }
                        ?></button>
                </form>

                </form>


        </nav>
    </body>
</html>