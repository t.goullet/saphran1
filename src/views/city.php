<!DOCTYPE html>

<html lang="en">

    <!-- vue contenant un tableau composé des villes du pays séléctionné
        a l'accueil. Le nombre de résultat est limité a 10 résultats grace au paginateur.
    -->

    <head>
        <title>Liste des villes</title>
        <?php include('head.php'); ?>
        <link rel="stylesheet" href="/src/views/styles.css">
    </head>

    <body>
        <?php include('nav.php'); ?>

        <br><br>

        <h1 style="text-align : center;" class="mb-5">Détails pour le pays <?php echo $country->getName(); ?></h1>

        <div class="text-center mb-5">
            <img src="<?php
            if ($country->getImage1() == null) {
                echo $country->getImage2();
            } else {
                echo $country->getImage1();
            }
            ?>">
        </div>

        <div class="row justify-content-md-center">
            <div class="col-10">

                <table class="table table-sm">
                    <tbody>
                        <tr>
                            <th scope="col">id</th>
                            <th><?php echo $country->getCountry_Id(); ?></th>
                        </tr>
                    </tbody>
                    <tbody>
                        <tr>
                            <th scope="row">nom</th>
                            <td><?php echo $country->getName(); ?></td>

                        </tr>
                        <tr>
                            <th scope="row">Continent</th>
                            <td><?php echo $country->getContinent(); ?></td>

                        </tr>
                        <tr>
                            <th scope="row">Capital</th>
                            <td colspan="2"><?php echo $country->getCapital(); ?></td>

                        </tr>
                        <tr>
                            <th scope="row">Population</th>
                            <td colspan="2"><?php echo $country->getPopulation(); ?></td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>

        <div class="row justify-content-md-center">
            <div class="col-10">

                <table class="table table-striped">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">id</th>
                            <th scope="col">nom</th>
                            <th scope="col">district</th>
                            <th scope="col">population</th>
                            
                            <!-- si l'id de role de l'utilisateur est égal a 1 ou 2 il pourra ajouter une ville -->
                            
                            <?php
                            if (isset($_SESSION['User'])) {
                                if ($_SESSION['User']->getUser_Roles_Id() == 1 or $_SESSION['User']->getUser_Roles_Id() == 2) {
                                    ?>
                                    <th scope="col"><a href="/city/show_add_city?CountryCode=<?php echo $country->getCode() ?>">Ajouter</a></th>
                                    <?php
                                }
                            }
                            ?>
                        </tr>
                    </thead>

                    <tbody>

                        <?php
                        $scope = $page * 10 - 10;
                        for ($indent = $page * 10 - 10; $indent < ceil($page * 10); $indent++): $scope++;
                            ?>
                            <?php if (isset($city[$indent])): ?>

                                <tr>
                                    <th scope="row"><?= $scope; ?></th>
                                    <td><?php echo $city[$indent]['City_Id']; ?></a></td>
                                    <td><?php echo $city[$indent]['Name']; ?></td>
                                    <td><?php echo $city[$indent]['District']; ?></td>
                                    <td><?php echo $city[$indent]['Population']; ?></td>
                                    
                                    <!-- si l'id de role de l'utilisateur est égal a 1 ou 2 il pourra modifier une ville -->
                                    
                                        <?php if (isset($_SESSION['User'])) {
                                           if ($_SESSION['User']->getUser_Roles_Id() == 1 or $_SESSION['User']->getUser_Roles_Id() == 2) {
                                       ?>
                                            <td><a href="/city/edit_city/<?php echo $city[$indent]['City_Id']; ?>">
                                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pen" fill="currentColor" xmlns="http://www.w3.org/2000/svg" style="color: green">
                                                    <path fill-rule="evenodd" d="M13.498.795l.149-.149a1.207 1.207 0 1 1 1.707 1.708l-.149.148a1.5 1.5 0 0 1-.059 2.059L4.854 14.854a.5.5 0 0 1-.233.131l-4 1a.5.5 0 0 1-.606-.606l1-4a.5.5 0 0 1 .131-.232l9.642-9.642a.5.5 0 0 0-.642.056L6.854 4.854a.5.5 0 1 1-.708-.708L9.44.854A1.5 1.5 0 0 1 11.5.796a1.5 1.5 0 0 1 1.998-.001zm-.644.766a.5.5 0 0 0-.707 0L1.95 11.756l-.764 3.057 3.057-.764L14.44 3.854a.5.5 0 0 0 0-.708l-1.585-1.585z"/>
                                                    </svg>
                                                </a>
                                                
                                                <!-- si l'id de role de l'utilisateur est égal a 1 il pourra supprimer une ville -->
                                                
                                                <?php
                                                    } if ($_SESSION['User']->getUser_Roles_Id() == 1) {
                                                ?>
                                                <a href="/city/delete_city/<?php echo $city[$indent]['City_Id']; ?>">
                                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trash" fill="currentColor" xmlns="http://www.w3.org/2000/svg" style="color: red">
                                                    <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                                    <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                                </a>
                                            </td>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tr>

                            <?php endif; ?>
<?php endfor; ?>

                    </tbody>

                </table>
                <br>
                <div class="row justify-content-md-center">
                    <div class="col col-lg-3">
                        <nav aria-label="...">
<?php echo $paginator; ?>
                        </nav>
                    </div>
                </div>

            </div>

        </div>
    </body>
</html>

