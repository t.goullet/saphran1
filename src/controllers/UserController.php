<?php

require_once '../src/utils/CsrfToken.php';
require_once '../src/utils/Auth.php';
require_once '../src/controllers/BaseController.php';
require_once '../src/utils/Renderer.php';
require_once '../src/model/DAOUser.php';
require_once '../src/utils/CountryVisitor.php';
require_once '../src/utils/filterer.php';

/**
 * Description of UserController
 *
 * @author student
 */
class UserController extends BaseController {

    /** Trait CsrfToken */
    use CsrfToken;

    /** @var DAOUser $daouser */
    private $daouser;

    /** @var CountryVisitor $visitorCountry */
    private $visitorUser;

    public function __construct() {
        $this->daouser = new DAOUser(SingletonDatabase::getInstance()->cnx);
        $this->visitorUser = new UserVisitor();
    }

    /**
     * Affichage de la page connexion
     */
    public function show_connexion_user() {
        $csrf_token = $this->generateToken();
        $view = Renderer::render("connexion.php", compact('csrf_token'));
        echo $view;
    }

    /**
     * verification de la connexion
     */
    public function check_connexion() {
        if (isset($_POST['btnconnexion'])) {
            $mail = htmlspecialchars($_POST["Mail"]);
            $password = htmlspecialchars(hash("sha512", $_POST['Password']));
            echo $password;
            $passwordverify = $this->daouser->findPasswordByMail($mail);
            var_dump($passwordverify);
            echo $passwordverify->getPassword();
            if ($password == $passwordverify->getPassword()) {
                $requete = $this->daouser->connexion($mail, $password);
                if ($requete != null) {
                    Auth::login($requete);
                } else {
                    echo 'utilisateur ou mot de passe incorrect';
                }
            } else {
                echo 'utilisateur ou mot de passe vide';
            }
            $view = Renderer::render("connexion.php");
            echo $view;
        }
    }

    /**
     * Déconnecte la personne connecté
     */
    public function check_deconnexion() {
        Auth::logout();
    }

    /**
     * Affichage de la page d'ajout d'user
     */
    public function show_add_user() {
        $csrf_token = $this->generateToken();
        $view = Renderer::render("adduser.php", compact('csrf_token'));
        echo $view;
    }
    
    /**
     * verification de l'inscription
     * @return type
     */
    public function check_inscription() {
        $key = htmlspecialchars($_POST['csrf_token']);
        $check = $this->check($key);
        if ($check) {
            if (isset($_POST['btnadd'])) {
                $data = [];
                $data["User_Id"] = htmlspecialchars($_POST['Id']);
                $data["Mail"] = htmlspecialchars($_POST['Mail']);
                $data["Nom"] = htmlspecialchars($_POST['Nom']);
                $data["Prenom"] = htmlspecialchars($_POST['Prenom']);
                $data["Password"] = htmlspecialchars(hash("sha512", $_POST['Password']));
                $data["User_Roles_Id"] = htmlspecialchars($_POST['User_Roles_Id']);

                $user = new User($data);

                $user->setUser_Id($data["User_Id"]);
                $user->setMail($data["Mail"]);
                $user->setNom($data["Nom"]);
                $user->setPrenom($data["Prenom"]);
                $user->setPassword($data["Password"]);
                $user->setUser_Roles_Id($data["User_Roles_Id"]);
                //     $this->daouser->save($user);
            }
            $filterer = new filterer($data);
            $filterer->acceptVisitor("Mail", $this->visitorUser);
            $filterer->acceptVisitor("Nom", $this->visitorUser);
            $filterer->acceptVisitor("Prenom", $this->visitorUser);
            $filterer->acceptVisitor("Password", $this->visitorUser);

            if ($filterer->Visit() == false) {
                $csrf_token = $this->generateToken();
                $view = Renderer::render("edituser.php", compact('user', 'csrf_token'));
                echo $view;
                return null;
            }
            $this->daouser->save($user);
            header("Location: /user/show_add_user");
        }
    }

    /**
     * Affichage de la page de modification d'un utilisateur
     * @param type $id de l'utilisateur
     */
    public function show_edit_user($id) {
        $user = $this->daouser->find($id);
        $csrf_token = $this->generateToken();
        $view = Renderer::render("edituser.php", compact('user', 'csrf_token'));
        echo $view;
    }

    /**
     * Mise à jour d'un utilisateur
     */
    public function update_user() {
        $key = htmlspecialchars($_POST['csrf_token']);
        $check = $this->check($key);
        if ($check) {
            if (isset($_POST['btnupdate'])) {
                $data = [];
                $data["User_Id"] = htmlspecialchars($_POST['id']);
                $data["Mail"] = htmlspecialchars($_POST['mail']);
                $data["Nom"] = htmlspecialchars($_POST['nom']);
                $data["Prenom"] = htmlspecialchars($_POST['prenom']);
                $data["Password"] = htmlspecialchars(password_hash($_POST['Password'], PASSWORD_BCRYPT));

                $country = new User($data);

                $country->setUser_Id($data["User_Id"]);
                $country->setMail($data["Mail"]);
                $country->setNom($data["Nom"]);
                $country->setPrenom($data["Prenom"]);
                $country->setPassword($data["Password"]);
                $csrf_token = $this->generateToken();
                //  $this->daouser->update($user);
            }
            $filterer = new filterer($data);
            $filterer->acceptVisitor("Mail", $this->visitorCity);
            $filterer->acceptVisitor("Nom", $this->visitorCity);
            $filterer->acceptVisitor("Prenom", $this->visitorCity);
            $filterer->acceptVisitor("Password", $this->visitorCity);

            if ($filterer->Visit() == false) {
                $csrf_token = $this->generateToken();
                $view = Renderer::render("edituser.php", compact('user', 'csrf_token'));
                echo $view;
                return null;
            }
            $this->daouser->update($user);
            header("Location: user/show_add_user");
        }
    }

    /**
     * Supression d'un utilisateur
     * @param type $id
     */
    public function delete_user($id) {
        $delete = $this->daouser->remove($id);
        if (isset($_POST['btndelete'])) {
            header('Location: /');
        }
    }

}
