<?php

require_once '../src/controllers/BaseController.php';
require_once '../src/utils/Renderer.php';
require_once '../src/utils/Paginator.php';
require_once '../src/model/DAOCountry.php';
require_once '../src/model/DAOCity.php';
require_once '../src/utils/CsrfToken.php';
require_once '../src/utils/CityVisitor.php';
require_once '../src/utils/filterer.php';

/**
 * Description of CityController
 *
 * @author student
 */
class CityController extends BaseController {

    /** Trait CsrfToken */
    use CsrfToken;

/** @var DAOCity $daocity */
    use Paginator;

    private $limit = 10;
    private $daocity;

    /** @var DAOCountry $daocountry */
    private $daocountry;

    /** @var CountryVisitor $visitorCountry */
    private $visitorCity;

    public function __construct() {
        $this->daocity = new DAOCity(SingletonDataBase::getInstance()->cnx);
        $this->daocountry = new DAOCountry(SingletonDatabase::getInstance()->cnx);
        $this->visitorCity = new CityVisitor();
    }

    /**
     * Affichage de la page de toutes les villes
     * @param type $name
     */
    public function show_city($name) {

        $city = $this->daocity->findCityByCountry($code);

        $url = rtrim("?page=", $_SERVER['REQUEST_URI']);
        if (isset($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }
        $count = $this->daocity->count($code);
        $paginator = $this->paginate($url, $page, $count, $this->limit);

        $view = Renderer::render("city.php", compact('city', 'paginator', 'page'));
        echo $view;
    }

    /**
     * afiichage de la page d'ajour d'une ville
     */
    public function show_add_city() {
        $csrf_token = $this->generateToken();
        $view = Renderer::render("addcity.php", compact('csrf_token'));
        echo $view;
    }

    /**
     * Ajout d'une ville avec le filtrer
     * @return type
     */
    public function add_city() {
        $key = htmlspecialchars($_POST['csrf_token']);
        $check = $this->check($key);
        if ($check) {
            if (isset($_POST['btnadd'])) {
                $data = [];
                $data["City_Id"] = htmlspecialchars($_POST['Id']);
                $data["CountryCode"] = htmlspecialchars($_POST['CountryCode']);
                $data["Name"] = htmlspecialchars($_POST['Name']);
                $data["District"] = htmlspecialchars($_POST['District']);
                $data["Population"] = htmlspecialchars($_POST['Population']);

                $city = new City($data);

                $city->setCity_Id($data["City_Id"]);
                $city->setCountryCode($data["CountryCode"]);
                $city->setName($data["Name"]);
                $city->setDistrict($data["District"]);
                $city->setPopulation($data["Population"]);
            }
            $filterer = new filterer($data);
            $filterer->acceptVisitor("Name", $this->visitorCity);
            $filterer->acceptVisitor("District", $this->visitorCity);
            $filterer->acceptVisitor("Population", $this->visitorCity);

            if ($filterer->Visit() == false) {
                $csrf_token = $this->generateToken();
                $view = Renderer::render("addcity.php", compact('csrf_token', 'ville'));
                echo $view;
                return null;
            }
            $this->daocity->save($city);
            $country = $this->daocountry->findCountryByCode($city->getCountryCode());
            $url = "/country/show_city/" . $country->getName() . "?page=1";
            header("Location: $url");
        }
    }

    /**
     * Affichage de la page d'edition d'une ville
     * @param type $id
     */
    public function edit_city($id) {
        $city = $this->daocity->find($id);
        $csrf_token = $this->generateToken();
        $view = Renderer::render("editcity.php", compact('city', 'csrf_token'));
        echo $view;
    }

    /**
     * Modification et mise à jour d'une ville avec le filtrer
     * @return type
     */
    public function update_city() {
        $key = htmlspecialchars($_POST['csrf_token']);
        $check = $this->check($key);
        if ($check) {
            if (isset($_POST['btnupdate'])) {
                $data = [];
                $data["City_Id"] = htmlspecialchars($_POST['id']);
                $data["Name"] = htmlspecialchars($_POST['Name']);
                $data["District"] = htmlspecialchars($_POST['District']);
                $data["Population"] = htmlspecialchars($_POST['Population']);

                $city = new City($data);

                $city->setCity_Id($data["City_Id"]);
                $city->setName($data["Name"]);
                $city->setDistrict($data["District"]);
                $city->setPopulation($data["Population"]);
                $csrf_token = $this->generateToken();
            }
            $filterer = new filterer($data);
            $filterer->acceptVisitor("Name", $this->visitorCity);
            $filterer->acceptVisitor("District", $this->visitorCity);
            $filterer->acceptVisitor("Population", $this->visitorCity);
            if ($filterer->Visit() == false) {
                $csrf_token = $this->generateToken();
                $view = Renderer::render("editcity.php", compact('csrf_token', 'ville'));
                echo $view;
                return null;
            }
            $this->daocity->update($city);
            header("Location: /");
        }
    }

    /**
     * Supression d'une ville
     * @param type $id
     */
    public function delete_city($id) {
        $delete = $this->daocity->remove($id);
        header("Location: /");
    }

}
