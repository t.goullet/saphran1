<?php

require_once '../src/controllers/BaseController.php';
require_once '../src/utils/Renderer.php';
require_once '../src/utils/Paginator.php';
require_once '../src/model/DAOCountry.php';
require_once '../src/model/DAOCity.php';
require_once '../src/utils/CsrfToken.php';
require_once '../src/utils/CountryVisitor.php';
require_once '../src/utils/filterer.php';

/**
 * Description of CountryController
 *
 * @author student
 */
class CountryController extends BaseController {

    /** Trait CsrfToken */
    use CsrfToken;

/** Trait Paginator */
    use Paginator;

    /** @var DAOCity $daoCity */
    private $daoCity;

    /** @var DAOCountry $daocountry */
    private $daocountry;

    /** @var CountryVisitor $visitorCountry */
    private $visitorCountry;

    /**
     * limite d'item par page pour le paginator
     * @var type 
     */
    private $limit = 10;

    public function __construct() {
        $this->daocountry = new DAOCountry(SingletonDatabase::getInstance()->cnx);
        $this->daoCity = new DAOCity(SingletonDataBase::getInstance()->cnx);
        $this->visitorCountry = new CountryVisitor();
    }

    /**
     * Affichage de tout les pays
     * @param type $id
     */
    public function find_country($id) {
        $country = $this->daocountry->find($id);
        $view = Renderer::render("accueil.php", compact('country'));
        echo $view;
    }

    /**
     * Affichage de tout les pays d'un continent
     * @param type $Name
     */
    public function show_country($Name) {
        $country = $this->daocountry->findCountriesByContinent($Name);
        $url = rtrim("?page=", $_SERVER['REQUEST_URI']);
        if (isset($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }
        $count = $this->daocountry->count($Name);
        $paginator = $this->paginate($url, $page, $count, $this->limit);
        $view = Renderer::render("country.php", compact('country', 'paginator', 'page'));
        echo $view;
    }

    /**
     * affichage de la page d'ajout d'un pays
     */
    public function show_add_country() {
        $csrf_token = $this->generateToken();
        $view = Renderer::render("addcountry.php", compact('csrf_token'));
        echo $view;
    }

    /**
     * Ajout d'un pays avec le filtre
     */
    public function add_country() {
        $key = htmlspecialchars($_POST['csrf_token']);
        $check = $this->check($key);
        if ($check) {
            if (isset($_POST['btnadd'])) {
                $data = [];
                $data["Country_Id"] = htmlspecialchars($_POST['id']);
                $data["Code"] = htmlspecialchars($_POST['code']);
                $data["Name"] = htmlspecialchars($_POST['nom']);
                $data["Continent"] = htmlspecialchars($_POST['continent']);
                $data["Region"] = htmlspecialchars($_POST['region']);
                $data["Population"] = htmlspecialchars($_POST['population']);
                $data["Capital"] = htmlspecialchars($_POST['capital']);
                $data['SurfaceArea'] = htmlspecialchars($_POST['surfaceArea']);
                $data['GNP'] = htmlspecialchars($_POST['gnp']);
                $data['GNPOld'] = htmlspecialchars($_POST['gnpOld']);
                $data['LocalName'] = htmlspecialchars($_POST['localName']);
                $data['GovernmentForm'] = htmlspecialchars($_POST['governmentForm']);
                $data['HeadOfState'] = htmlspecialchars($_POST['headOfState']);
                $data['IndepYear'] = htmlspecialchars($_POST['indepYear']);
                $data['Code2'] = htmlspecialchars($_POST['code2']);
                $data['LifeExpectancy'] = htmlspecialchars($_POST['lifeExpectancy']);
                $data['Image1'] = htmlspecialchars($_POST['image1']);
                $data['Image2'] = htmlspecialchars($_POST['image2']);

                $country = new Country($data);

                $country->setCountry_Id($data["Country_Id"]);
                $country->setCode($data["Code"]);
                $country->setName($data["Name"]);
                $country->setContinent($data["Continent"]);
                $country->setRegion($data["Region"]);
                $country->setPopulation($data["Population"]);
                $country->setCapital($data["Capital"]);
                $country->setSurfaceArea($data["SurfaceArea"]);
                $country->setGNP($data["GNP"]);
                $country->setGNPOld($data["GNPOld"]);
                $country->setLocalName($data["LocalName"]);
                $country->setGovernmentForm($data["GovernmentForm"]);
                $country->setHeadOfState($data["HeadOfState"]);
                $country->setIndepYear($data["IndepYear"]);
                $country->setCode2($data["Code2"]);
                $country->setLifeExpectancy($data["LifeExpectancy"]);
                $country->setImage1($data["Image1"]);
                $country->setImage2($data["Image2"]);
                $this->daocountry->save($country);
            }

            $filterer = new filterer($data);
            $filterer->acceptVisitor("Code", $this->visitorCountry);
            $filterer->acceptVisitor("Name", $this->visitorCountry);
            $filterer->acceptVisitor("Region", $this->visitorCountry);
            $filterer->acceptVisitor("SurfaceArea", $this->visitorCountry);
            $filterer->acceptVisitor("Population", $this->visitorCountry);
            $filterer->acceptVisitor("GNP", $this->visitorCountry);
            $filterer->acceptVisitor("LocalName", $this->visitorCountry);
            $filterer->acceptVisitor("GovernmentForm", $this->visitorCountry);
            $filterer->acceptVisitor("Capital", $this->visitorCountry);
            $filterer->acceptVisitor("Code2", $this->visitorCountry);

            if ($filterer->Visit() == false) {
                $csrf_token = $this->generateToken();
                $view = Renderer::render("addCountry.php", compact('csrf_token', 'pays'));
                echo $view;
                return null;
            }
            $this->daocountry->save($country);
            $url = "/country/show_country/" . $country->getContinent() . "?page=1";
            header("Location: $url");
        }
    }

    /**
     * Affichage de la page d'edition d'un pays
     * @param type $id
     */
    public function edit_country($name) {
        $country = $this->daocountry->find($name);
        $csrf_token = $this->generateToken();
        $view = Renderer::render("editcountry.php", compact('country', 'csrf_token'));
        echo $view;
    }

    /**
     * Mise à jour d'un pays avec le filtre
     * 
     */
    public function update_country() {
        $key = htmlspecialchars($_POST['csrf_token']);
        $check = $this->check($key);
        if ($check) {
            if (isset($_POST['btnupdate'])) {
                $data = [];
                $data["Country_Id"] = htmlspecialchars($_POST['id']);
                $data["Code"] = htmlspecialchars($_POST['code']);
                $data["Name"] = htmlspecialchars($_POST['nom']);
                $data["Continent"] = htmlspecialchars($_POST['continent']);
                $data["Region"] = htmlspecialchars($_POST['region']);
                $data["Population"] = htmlspecialchars($_POST['population']);
                $data["Capital"] = htmlspecialchars($_POST['capital']);
                $data['SurfaceArea'] = htmlspecialchars($_POST['surfaceArea']);
                $data['GNP'] = htmlspecialchars($_POST['gnp']);
                $data['GNPOld'] = htmlspecialchars($_POST['gnpOld']);
                $data['LocalName'] = htmlspecialchars($_POST['localName']);
                $data['GovernmentForm'] = htmlspecialchars($_POST['governmentForm']);
                $data['HeadOfState'] = htmlspecialchars($_POST['headOfState']);
                $data['IndepYear'] = htmlspecialchars($_POST['indepYear']);
                $data['Code2'] = htmlspecialchars($_POST['code2']);
                $data['LifeExpectancy'] = htmlspecialchars($_POST['lifeExpectancy']);
                $data['Image1'] = htmlspecialchars($_POST['image1']);
                $data['Image2'] = htmlspecialchars($_POST['image2']);

                $country = new Country($data);

                $country->setCountry_Id($data["Country_Id"]);
                $country->setCode($data["Code"]);
                $country->setName($data["Name"]);
                $country->setContinent($data["Continent"]);
                $country->setRegion($data["Region"]);
                $country->setPopulation($data["Population"]);
                $country->setCapital($data["Capital"]);
                $country->setSurfaceArea($data["SurfaceArea"]);
                $country->setGNP($data["GNP"]);
                $country->setGNPOld($data["GNPOld"]);
                $country->setLocalName($data["LocalName"]);
                $country->setGovernmentForm($data["GovernmentForm"]);
                $country->setHeadOfState($data["HeadOfState"]);
                $country->setIndepYear($data["IndepYear"]);
                $country->setCode2($data["Code2"]);
                $country->setLifeExpectancy($data["LifeExpectancy"]);
                $country->setImage1($data["Image1"]);
                $country->setImage2($data["Image2"]);
                $csrf_token = $this->generateToken();
            }
            $filterer = new filterer($data);
            $filterer->acceptVisitor("Code", $this->visitorCountry);
            $filterer->acceptVisitor("Name", $this->visitorCountry);
            $filterer->acceptVisitor("Region", $this->visitorCountry);
            $filterer->acceptVisitor("SurfaceArea", $this->visitorCountry);
            $filterer->acceptVisitor("Population", $this->visitorCountry);
            $filterer->acceptVisitor("GNP", $this->visitorCountry);
            $filterer->acceptVisitor("LocalName", $this->visitorCountry);
            $filterer->acceptVisitor("GovernmentForm", $this->visitorCountry);
            $filterer->acceptVisitor("Capital", $this->visitorCountry);
            $filterer->acceptVisitor("Code2", $this->visitorCountry);

            if ($filterer->Visit() == false) {
                $csrf_token = $this->generateToken();
                $view = Renderer::render("addCountry.php", compact('csrf_token', 'pays'));
                echo $view;
                return null;
            }
            $this->daocountry->update($country);
            $url = "/country/show_country/" . $country->getContinent() . "?page=1";
            header("Location: $url");
        }
    }
    
    /**
     * supression d'un pays
     * @param type $id
     */
    public function delete_country($id) {
        $delete = $this->daocountry->remove($id);
        header('Location: /');
    }

    /**
     * Affichage de la page de toutes les villes
     * @param type $name
     */
    public function show_city($name) {
        $daocity = new DAOCity(SingletonDataBase::getInstance()->cnx);
        $country = $this->daocountry->find($name);
        $city = $daocity->findCityByCountry($country->getCode());

        $url = rtrim("?page=", $_SERVER['REQUEST_URI']);
        if (isset($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }
        $count = $this->daocountry->count($name);
        $paginator = $this->paginate($url, $page, $count, $this->limit);

        $view = Renderer::render("city.php", compact('city', 'country', 'paginator', 'page'));
        echo $view;
    }

}
