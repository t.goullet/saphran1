<?php

require_once 'BaseController.php';
require_once '../src/utils/SingletonDataBase.php';

/**
 * Description of DefaultController
 *
 * @author student
 */
class DefaultController extends BaseController {

    /** @var DAOCountry $daocountry */
    private $daocountry;

    public function __construct() {
        $this->daocountry = new DAOCountry(SingletonDatabase::getInstance()->cnx);
    }

    /**
     * Affichage de tout les continent distinct des pays
     */
    public function show_continent() {
        //verifier la validité de id
        $continent = $this->daocountry->get_enum_from_continent();
        $view = Renderer::render("accueil.php", compact('continent'));
        echo $view;
    }

}
