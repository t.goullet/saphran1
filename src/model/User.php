<?php

require_once '../src/model/Model.php';

/**
 * Getter et Setter de user
 */
class User extends Model {

    protected $User_Id;
    protected $Mail;
    protected $Password;
    protected $Nom;
    protected $Prenom;
    protected $User_Roles_Id;

    /**
     * User constructor
     * @param array $data
     */
    public function __construct(array $data = NULL) {
        parent::__construct();
        if ($data != NULL) {
            foreach ($data as $key => $value) {
                $method = "set" . $key;
                if (method_exists($this, $method)) {
                    $this->$key = $value;
                }
            }
        }
    }

    function getUser_Id() {
        return $this->User_Id;
    }

    function getMail() {
        return $this->Mail;
    }

    function getPassword() {
        return $this->Password;
    }

    function getNom() {
        return $this->Nom;
    }

    function getPrenom() {
        return $this->Prenom;
    }

    function getUser_Roles_Id() {
        return $this->User_Roles_Id;
    }

    function setUser_Id($User_Id): void {
        $this->User_Id = $User_Id;
    }

    function setMail($Mail): void {
        $this->Mail = $Mail;
    }

    function setPassword($Password): void {
        $this->Password = $Password;
    }

    function setNom($Nom): void {
        $this->Nom = $Nom;
    }

    function setPrenom($Prenom): void {
        $this->Prenom = $Prenom;
    }

    function setUser_Roles_Id($User_Roles_Id): void {
        $this->User_Roles_Id = $User_Roles_Id;
    }

}
