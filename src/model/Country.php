<?php

require_once '../src/model/Model.php';

/**
 * Getter et Setter de Country
 */
class Country extends Model {

    protected $Country_Id;
    protected $Code;
    protected $Name;
    protected $Continent;
    protected $Region;
    protected $SurfaceArea;
    protected $IndepYear;
    protected $Population;
    protected $LifeExpectancy;
    protected $GNP;
    protected $GNPOld;
    protected $LocalName;
    protected $GovernmentForm;
    protected $HeadOfState;
    protected $Capital;
    protected $Code2;
    protected $Image1;
    protected $Image2;

    /**
     * Country constructor
     * @param array $data
     */
    public function __construct(array $data = NULL) {
        parent::__construct();
        if ($data != NULL) {
            foreach ($data as $key => $value) {
                $method = "set" . $key;
                if (method_exists($this, $method)) {
                    $this->$key = $value;
                }
            }
        }
    }

    function getCountry_Id() {
        return $this->Country_Id;
    }

    function getCode() {
        return $this->Code;
    }

    function getName() {
        return $this->Name;
    }

    function getContinent() {
        return $this->Continent;
    }

    function getRegion() {
        return $this->Region;
    }

    function getSurfaceArea() {
        return $this->SurfaceArea;
    }

    function getIndepYear() {
        return $this->IndepYear;
    }

    function getPopulation() {
        return $this->Population;
    }

    function getLifeExpectancy() {
        return $this->LifeExpectancy;
    }

    function getGNP() {
        return $this->GNP;
    }

    function getGNPOld() {
        return $this->GNPOld;
    }

    function getLocalName() {
        return $this->LocalName;
    }

    function getGovernmentForm() {
        return $this->GovernmentForm;
    }

    function getHeadOfState() {
        return $this->HeadOfState;
    }

    function getCapital() {
        return $this->Capital;
    }

    function getCode2() {
        return $this->Code2;
    }

    function getImage1() {
        return $this->Image1;
    }

    function getImage2() {
        return $this->Image2;
    }

    function setCountry_Id($Country_Id): void {
        $this->Country_Id = $Country_Id;
    }

    function setCode($Code): void {
        $this->Code = $Code;
    }

    function setName($Name): void {
        $this->Name = $Name;
    }

    function setContinent($Continent): void {
        $this->Continent = $Continent;
    }

    function setRegion($Region): void {
        $this->Region = $Region;
    }

    function setSurfaceArea($SurfaceArea): void {
        $this->SurfaceArea = $SurfaceArea;
    }

    function setIndepYear($IndepYear): void {
        $this->IndepYear = $IndepYear;
    }

    function setPopulation($Population): void {
        $this->Population = $Population;
    }

    function setLifeExpectancy($LifeExpectancy): void {
        $this->LifeExpectancy = $LifeExpectancy;
    }

    function setGNP($GNP): void {
        $this->GNP = $GNP;
    }

    function setGNPOld($GNPOld): void {
        $this->GNPOld = $GNPOld;
    }

    function setLocalName($LocalName): void {
        $this->LocalName = $LocalName;
    }

    function setGovernmentForm($GovernmentForm): void {
        $this->GovernmentForm = $GovernmentForm;
    }

    function setHeadOfState($HeadOfState): void {
        $this->HeadOfState = $HeadOfState;
    }

    function setCapital($Capital): void {
        $this->Capital = $Capital;
    }

    function setCode2($Code2): void {
        $this->Code2 = $Code2;
    }

    function setImage1($Image1): void {
        $this->Image1 = $Image1;
    }

    function setImage2($Image2): void {
        $this->Image2 = $Image2;
    }

}
