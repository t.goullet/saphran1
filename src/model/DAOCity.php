<?php

require_once '../src/model/City.php';
require_once '../src/model/DAO.php';

/**
 * Description of DAOCity
 * 
 * @author student
 */
class DAOCity extends DAO {

    public $cnx;

    public function __construct(PDO $cnx) {
        parent::__construct($cnx);
    }

    /**
     * @param type $id
     * @return \City l'id d'une ville
     */
    public function find($id): City {
        $SQL = "SELECT * FROM city WHERE City_Id= :Id";
        $preparedStatement = $this->cnx->prepare($SQL);
        $preparedStatement->bindValue("Id", $id);
        $preparedStatement->execute();
        $city = $preparedStatement->fetchObject("City");
        return $city;
    }

    /**
     * Insert une ville dans la Bdd
     * @param type $city
     */
    function save($city) {
        $SQL = "INSERT INTO city (CountryCode, Name, District, Population) VALUES (:CountryCode, :Name, :District, :Population)";
        $preparedStatement = $this->cnx->prepare($SQL);
        $preparedStatement->bindValue("CountryCode", $city->getCountryCode());
        $preparedStatement->bindValue("Name", $city->getName());
        $preparedStatement->bindValue("District", $city->getDistrict());
        $preparedStatement->bindValue("Population", $city->getPopulation());
        $preparedStatement->execute();
    }

    /**
     * Modifie une ville dans la Bdd
     * @param type $city
     */
    function update($city) {
        $SQL = "UPDATE city SET Name = :Name, District = :District, Population = :Population WHERE City_Id = :id";
        $preparedStatement = $this->cnx->prepare($SQL);
        $preparedStatement->bindValue("id", $city->getCity_Id());
        $preparedStatement->bindValue("Name", $city->getName());
        $preparedStatement->bindValue("District", $city->getDistrict());
        $preparedStatement->bindValue("Population", $city->getPopulation());
        $preparedStatement->execute();
    }

    /**
     * Supprime une ville dans la Bdd
     * @param type $id
     * @return void
     */
    function remove($id): void {
        $SQL = "DELETE FROM city WHERE City_Id = :id";
        $prepareStatement = $this->cnx->prepare($SQL);
        $prepareStatement->bindValue("id", $id);
        $prepareStatement->execute();
    }

    /**
     * Selectionne toutes les villes de la Bdd
     * @return array
     */
    public function findAll(): array {
        $SQL = "SELECT * FROM city LIMIT 10";
        $prepareStatement = $this->cnx->prepare($SQL);
        $prepareStatement->execute;
        $prepareStatement->setFetchMode(PDO::FETCH_CLASS, 'City');
        $city_list = [];
        while (($data = $prepareStatement->fetchObject("City")) != false) {
            array_push($city_list, $data);
        }
        return $city_list;
    }

    /**
     * Selectionne le nombre de ville
     * @return int
     */
    public function count(): int {
        $SQL = "SELECT COUNT(City_id) FROM city";
        $prepareStatement = $this->cnx->query($SQL);
        $prepareStatement->execute();
        $country_count = $prepareStatement->fetchColumn();
        return $country_count;
    }

    /**
     * Selectionne toutes les ville en fonction du pays
     * @param type $code
     * @return type
     */
    public function findCityByCountry($code) {
        $SQL = "SELECT * FROM city WHERE CountryCode = :CountryCode";
        $preparedStatement = $this->cnx->prepare($SQL);
        $preparedStatement->bindValue("CountryCode", $code);
        $preparedStatement->execute();
        $findCity = $preparedStatement->fetchAll(PDO::FETCH_ASSOC);
        return $findCity;
    }

}
