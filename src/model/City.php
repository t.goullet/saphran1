<?php

require_once '../src/model/Model.php';

/**
 * getter et setter de City
 */
class City extends Model {

    protected $City_Id;
    protected $Name;
    protected $CountryCode;
    protected $District;
    protected $Population;

    /**
     * City constructor
     * @param array $data
     */
    public function construct(array $data = NULL) {
        parent::__construct();
        if ($data != NULL) {
            $this->City_Id = $data["City_Id"];
            $this->Name = $data["Name"];
            $this->CountryCode = $data["CountryCode"];
            $this->District = $data["District"];
            $this->Population = $data["Population"];
        }
    }

    function getCity_Id() {
        return $this->City_Id;
    }

    function getName() {
        return $this->Name;
    }

    function getCountryCode() {
        return $this->CountryCode;
    }

    function getDistrict() {
        return $this->District;
    }

    function getPopulation() {
        return $this->Population;
    }

    function setCity_Id($City_Id): void {
        $this->City_Id = $City_Id;
    }

    function setName($Name): void {
        $this->Name = $Name;
    }

    function setCountryCode($CountryCode): void {
        $this->CountryCode = $CountryCode;
    }

    function setDistrict($District): void {
        $this->District = $District;
    }

    function setPopulation($Population): void {
        $this->Population = $Population;
    }

}
