<?php

require_once '../src/model/Country.php';
require_once '../src/model/DAO.php';

/**
 * Description of DAOCountry
 * 
 * @author student
 */
class DAOCountry extends DAO {

    public $cnx;

    public function __construct(PDO $cnx) {
        parent::__construct($cnx);
    }

    /**
     * Selectionne un pays en fonction de son nom dans la bdd
     * @param type $name le nom d'un pays
     * @return \Country|null
     */
    public function find($name): ?Country {
        $sql = "SELECT * FROM country WHERE Name = :Name";
        $preparedStatement = $this->cnx->prepare($sql);
        $preparedStatement->bindValue("Name", $name);
        $preparedStatement->execute();
        $country = $preparedStatement->fetchObject("Country");
        if ($country == "") {
            return NULL;
        }
        return $country;
    }

    /**
     * Selectionne un pays en fonction de son Id
     * @param type $Id l'id d'un pays
     * @return \Country|null
     */
    public function find_id($Id): ?Country {
        $sql = "SELECT * FROM country WHERE Country_Id = :Country_Id";
        $preparedStatement = $this->cnx->prepare($sql);
        $preparedStatement->bindValue("Country_Id", $Id);
        $preparedStatement->execute();
        $country = $preparedStatement->fetchObject("Country");
        if ($country == "") {
            return NULL;
        }
        return $country;
    }

    /**
     * Insert un pays dans la Bdd
     * @param type $country
     */
    function save($country) {
        $sql = "INSERT INTO country (Code, Name, Continent, Region, SurfaceArea, IndepYear, Population, LifeExpectancy, GNP, GNPOld, LocalName, GovernmentForm, HeadOfState, Capital, Code2, Image1, Image2) VALUES (:Code, :Name, :Continent, :Region, :SurfaceArea, :IndepYear, :Population, :LifeExpectancy, :GNP, :GNPOld, :LocalName, :GovernmentForm, :HeadOfState, :Capital, :Code2, :Image1, :Image2)";
        $preparedStatement = $this->cnx->prepare($sql);
        $preparedStatement->bindValue("Code", $country->getCode());
        $preparedStatement->bindValue("Name", $country->getName());
        $preparedStatement->bindValue("Continent", $country->getContinent());
        $preparedStatement->bindValue("Region", $country->getRegion());
        $preparedStatement->bindValue("SurfaceArea", $country->getSurfaceArea());
        $preparedStatement->bindValue("IndepYear", $country->getIndepYear());
        $preparedStatement->bindValue("Population", $country->getPopulation());
        $preparedStatement->bindValue("LifeExpectancy", $country->getLifeExpectancy());
        $preparedStatement->bindValue("GNP", $country->getGNP());
        $preparedStatement->bindValue("GNPOld", $country->getGNPOld());
        $preparedStatement->bindValue("LocalName", $country->getLocalName());
        $preparedStatement->bindValue("GovernmentForm", $country->getGovernmentForm());
        $preparedStatement->bindValue("HeadOfState", $country->getHeadOfState());
        $preparedStatement->bindValue("Capital", $country->getCapital());
        $preparedStatement->bindValue("Code2", $country->getCode2());
        $preparedStatement->bindValue("Image1", $country->getImage1());
        $preparedStatement->bindValue("Image2", $country->getImage2());
        $preparedStatement->execute();
    }

    /**
     * Modifie un pays dans la Bdd
     * @param type $country
     */
    
    function update($country) {
        $sql = "UPDATE country SET Code = :Code, Name = :Name, Continent = :Continent, Region = :Region, SurfaceArea = :SurfaceArea, IndepYear = :IndepYear, Population = :Population, LifeExpectancy = :LifeExpectancy, GNP = :GNP, GNPOld = :GNPOld, LocalName = :LocalName, GovernmentForm = :GovernmentForm, HeadOfState = :HeadOfState, Capital = :Capital, Code2 = :Code2, Image1 = :Image1, Image2 = :Image2 WHERE Country_Id = :Country_Id";
        $preparedStatement = $this->cnx->prepare($sql);
        $preparedStatement->bindValue("Country_Id", $country->getCountry_Id());
        $preparedStatement->bindValue("Code", $country->getCode());
        $preparedStatement->bindValue("Name", $country->getName());
        $preparedStatement->bindValue("Continent", $country->getContinent());
        $preparedStatement->bindValue("Region", $country->getRegion());
        $preparedStatement->bindValue("SurfaceArea", $country->getSurfaceArea());
        $preparedStatement->bindValue("IndepYear", $country->getIndepYear());
        $preparedStatement->bindValue("Population", $country->getPopulation());
        $preparedStatement->bindValue("LifeExpectancy", $country->getLifeExpectancy());
        $preparedStatement->bindValue("GNP", $country->getGNP());
        $preparedStatement->bindValue("GNPOld", $country->getGNPOld());
        $preparedStatement->bindValue("LocalName", $country->getLocalName());
        $preparedStatement->bindValue("GovernmentForm", $country->getGovernmentForm());
        $preparedStatement->bindValue("HeadOfState", $country->getHeadOfState());
        $preparedStatement->bindValue("Capital", $country->getCapital());
        $preparedStatement->bindValue("Code2", $country->getCode2());
        $preparedStatement->bindValue("Image1", $country->getImage1());
        $preparedStatement->bindValue("Image2", $country->getImage2());
        $code = $preparedStatement->execute();
    }

    /**
     * Supprime un pays dans la Bdd
     * @param type $id
     */
    function remove($id) {
        $sqlCity = "DELETE FROM city WHERE CountryCode = (SELECT Code FROM country WHERE Country_Id = :Country_Id)";
        $sqlLanguage = "DELETE FROM countrylanguage WHERE CountryCode = (SELECT Code FROM country WHERE Country_Id = :Country_Id)";
        $sqlCountry = "DELETE FROM country WHERE Country_Id = :id";
        $preparedStatementCity = $this->cnx->prepare($sqlCity);
        $preparedStatementLanguage = $this->cnx->prepare($sqlLanguage);
        $preparedStatementCountry = $this->cnx->prepare($sqlCountry);
        $preparedStatementCity->bindValue("Country_Id", $id);
        $preparedStatementCity->execute();
        $preparedStatementLanguage->bindValue("Country_Id", $id);
        $preparedStatementLanguage->execute();
        $preparedStatementCountry->bindValue("id", $id);
        $preparedStatementCountry->execute();
    }

    /**
     * Selectionne tout les pays de la bdd
     * @return array
     */
    public function findAll(): array {
        $SQL = "SELECT * FROM country";
        $preparedStatement = $this->cnx->query($SQL);
        $preparedStatement->setFetchMode(PDO::FETCH_CLASS, 'Country');
        $preparedStatement->execute();
        $country_list = [];
        while (($data = $preparedStatement->fetchObject("Country")) != false) {
            array_push($country_list, $data);
        }
        return $country_list;
    }

    /**
     * Selectionne tout pays en fonction du nom du continent
     * @param type $Name
     * @return type
     */
    public function findCountriesByContinent($Name) {
        $SQL = "SELECT * FROM country WHERE Continent= :Name";
        $preparedStatement = $this->cnx->prepare($SQL);
        $preparedStatement->bindValue("Name", $Name);
        $preparedStatement->execute();
        $findCountries = $preparedStatement->fetchAll(PDO::FETCH_ASSOC);
        return $findCountries;
    }

    /**
     * Selectionne le continent d'un pays
     * @return type
     */
    public function get_enum_from_continent() {
        $SQL = "SELECT DISTINCT continent FROM country";
        $preparedStatement = $this->cnx->query($SQL);
        $preparedStatement->execute();
        $enum = $preparedStatement->fetchAll(PDO::FETCH_ASSOC);
        return $enum;
    }

    /**
     * Selectione le nombre de pays
     * @return int
     */
    public function count(): int {
        $SQL = "SELECT COUNT(Country_Id) FROM country";
        $prepareStatement = $this->cnx->query($SQL);
        $prepareStatement->execute();
        $country_count = $prepareStatement->fetchColumn();
        return $country_count;
    }

    public function findCountryByCode($code): Country {
        $SQL = "SELECT * FROM country WHERE Code = :Code";
        $preparedStatement = $this->cnx->prepare($SQL);
        $preparedStatement->bindValue("Code", $code);
        $preparedStatement->execute();
        $countrycode = $preparedStatement->fetchObject("Country");
        return $countrycode;
        
       
    }

}
