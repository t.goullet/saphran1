<?php

require_once '../src/model/User.php';
require_once '../src/model/DAO.php';

/**
 * Description of DAOUser
 *
 * @author student
 */
class DAOUser extends DAO {

    public $cnx;

    public function __construct(PDO $cnx) {
        parent::__construct($cnx);
    }

    /**
     * Selectionne tout les utilisateur en fonction d'un mail et d'un mots de passe 
     * @param type $Mail
     * @param type $Password
     * @return type
     */
    public function connexion($Mail, $Password) {
        $sql = "SELECT * FROM users WHERE Mail = :Mail AND Password = :Password";
        $preparedStatement = $this->cnx->prepare($sql);
        $preparedStatement->bindValue("Mail", $Mail);
        $preparedStatement->bindValue("Password", $Password);
        $preparedStatement->execute();
        $user = $preparedStatement->fetchObject("user");
        if ($user == "") {
            return NULL;
        }
        return $user;
    }

    /**
     * selectionne un utilisateur en fonction de son Id 
     * @param type $id
     * @return \Users|null
     */
    public function find($id): ?Users {
        $sql = "SELECT * FROM users WHERE User_Id = :User_Id";
        $preparedStatement = $this->cnx->prepare($sql);
        $preparedStatement->bindValue("User_Id", $id);
        $preparedStatement->execute();
        $user = $preparedStatement->fetchObject("Users");
        if ($user == "") {
            return NULL;
        }
        return $user;
    }

    /**
     * Selectionne un utilisateur en fonction de sonn mail
     * @param type $mail
     * @return \Users|null
     */
    public function findPasswordByMail($mail) {
        $sql = "SELECT Password FROM users WHERE Mail = :Mail";
        $preparedStatement = $this->cnx->prepare($sql);
        $preparedStatement->bindValue("Mail", $mail);
        $preparedStatement->execute();
        $password = $preparedStatement->fetchObject("User");
        if ($password == "") {
            return NULL;
        }
        return $password;
    }

    /**
     * selctionne le nombre d'utilisateur 
     * @return int
     */
    function count(): int {
        $sql = "SELECT COUNT (User_Id) FROM users";
        $preparedStatement = $this->cnx->prepare($sql);
        $preparedStatement->execute();
        $count_user = $preparedStatement->fetchColumn();
        return $count_user;
    }

    /**
     * Insert un utilisateur dans la Bdd
     * @param type $user
     */
    function save($user) {
        $sql = "INSERT INTO users (Mail, Password, Nom, Prenom, User_Roles_Id) VALUES (:Mail, :Password, :Nom, :Prenom, :User_Roles_Id)";
        $preparedStatement = $this->cnx->prepare($sql);
        $preparedStatement->bindValue("Mail", $user->getMail());
        $preparedStatement->bindValue("Password", $user->getPassword());
        $preparedStatement->bindValue("Nom", $user->getNom());
        $preparedStatement->bindValue("Prenom", $user->getPrenom());
        $preparedStatement->bindValue("User_Roles_Id", $user->getUser_Roles_Id());
        $preparedStatement->execute();
    }

    /**
     * Modifie un utilisateur dans la Bdd
     * @param type $user
     */
    function update($user) {
        $sql = "UPDATE users SET Mail = :Mail, Nom = :Nom, Prenom = :Prenom, Password = :Password WHERE User_Id = :User_Id";
        $preparedStatement = $this->cnx->prepare($sql);
        $preparedStatement->bindValue("User_Id", $user->getUser_Id());
        $preparedStatement->bindValue("Mail", $user->getMail());
        $preparedStatement->bindValue("Nom", $user->getNom());
        $preparedStatement->bindValue("Prenom", $user->getPrenom());
        $preparedStatement->bindValue("Password", $user->getPassword());
        $preparedStatement->execute();
    }

    /**
     * supprime un utilisateur dans la Bdd
     * @param type $id
     */
    function remove($id) {
        $sqlUser = "DELETE FROM users WHERE User_Id = :id";
        $preparedStatementUser = $this->cnx->prepare($sqlUser);
        $preparedStatementUser > bindValue("id", $id);
        $preparedStatementUser->execute();
    }

    /**
     * Selectionne tout les utilisateurs
     * @return array
     */
    public function findAll(): array {
        $SQL = "SELECT * FROM users";
        $preparedStatement = $this->cnx->query($SQL);
        $preparedStatement->setFetchMode(PDO::FETCH_CLASS, 'User');
        $preparedStatement->execute();
        $user_list = [];
        while (($data = $preparedStatement->fetchObject("User")) != false) {
            array_push($user_list, $data);
        }
        return $user_list;
    }

}
