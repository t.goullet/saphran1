<?php

/**
 * Description of DAO
 *
 * @author student
 */
abstract class DAO {

    /** @var $cnx PDO */
    protected $cnx;

    public function __construct($cnx) {
        $this->cnx = $cnx;
    }

    abstract public function find($id);

    abstract public function save($city);

    abstract public function update($city);

    abstract public function remove($city);

    abstract public function findAll(): array;

    abstract public function count(): int;
}
