<?php

require_once '../src/utils/AbstractVisitor.php';

/**
 * Description of CountryVisitor
 *
 * @author student
 */
Class CountryVisitor extends AbstractVisitor {

    /** @var DAOCountry $daoCountry */
    private $daocountry;

    public function __construct() {
        $this->daocountry = new DAOCountry(SingletonDataBase::getInstance()->cnx);
    }

    /**
     * Vérifie que le Code de Country à bien les pré-requis nécessaires
     * @param string $data
     * @return bool
     */
    public function CountryCodeVisitor(string $data): bool {
        $code = (string) $data;
        if (strlen($code) == 3 && preg_match('@[A-Z]@', $code)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Vérifie que le nom de Country à bien les pré-requis nécessaires
     * @param string $data
     * @return bool
     */
    public function CountryNameVisitor(string $data): bool {
        $name = (string) $data;
        if (strlen($name) > 2 && strlen($name) < 60 && ctype_alpha($name)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Vérifie que le Code de la region de Country à bien les pré-requis nécessaires
     * @param string $data
     * @return bool
     */
    public function CountryRegionVisitor(string $data): bool {
        $region = (string) $data;
        if (strlen($region) > 2 && strlen($region) < 100 && ctype_alpha($region)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Vérifie que la surface de Country à bien les pré-requis nécessaires
     * @param int $data
     * @return bool
     */
    public function CountrySurfaceAreaVisitor(int $data): bool {
        $surfaceArea = (int) $data;
        if (strlen($surfaceArea) <= 19 && preg_match('@[0-9]@', $surfaceArea)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Vérifie que la population de Country à bien les pré-requis nécessaires
     * @param string $data
     * @return bool
     */
    public function CountryPopulationVisitor(int $data): bool {
        $pop = (int) $data;
        if (strlen($pop) <= 11 && preg_match('@[0-9]@', $pop)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Vérifie que le GNP de Country à bien les pré-requis nécessaires
     * @param string $data
     * @return bool
     */
    public function CountryGNPVisitor(int $data): bool {
        $gnp = (int) $data;
        if (strlen($gnp) <= 11 && preg_match('@[0-9]@', $gnp)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Vérifie que le nom local de Country à bien les pré-requis nécessaires
     * @param string $data
     * @return bool
     */
    public function CountryLocalNameVisitor(string $data): bool {
        $localName = (string) $data;
        if (strlen($name) > 2 && strlen($localName) < 60 && ctype_alpha($localName)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Vérifie que GovernmentForm de Country à bien les pré-requis nécessaires
     * @param string $data
     * @return bool
     */
    public function CountryGovernmentFormVisitor(string $data): bool {
        $governmentForm = (string) $data;
        if (strlen($name) > 2 && strlen($governmentForm) < 60 && ctype_alpha($governmentForm)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Vérifie que la capital de Country à bien les pré-requis nécessaires
     * @param string $data
     * @return bool
     */
    public function CountryCapitalVisitor(int $data): bool {
        $capital = (int) $data;
        if (strlen($capital) <= 6 && preg_match('@[0-9]@', $capital)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Vérifie que le Code2 de Country à bien les pré-requis nécessaires
     * @param string $data
     * @return bool
     */
    public function CountryCode2Visitor(string $data): bool {
        $code2 = (string) $data;
        if (strlen($code2) == 2 && preg_match('@[A-Z]@', $code2)) {
            return true;
        } else {
            return false;
        }
    }

}
