<?php

require_once '../vendor/autoload.php';

$faker = Faker\Factory::create('fr_FR');


/*
 * le fakerer va s'instancier lors de l'ajout d'un utilisateur pas encore inscrit
 * dans la base de données. Un email unique, un prenom et un nom seront générés
 * grace au fakerer dans le formulaire de la view adduser.php
 */

$fakerEmailDomain = $faker->safeEmailDomain;
$fakerNom = $faker->lastName;
$fakerPrenom = $faker->firstName;
$fakerPassword = $faker->password;