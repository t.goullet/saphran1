<?php

include_once '../src/utils/UserVisitor.php';

/**
 * Description of Filter
 *
 * @author student
 */
class filterer {

    private $visitors = [];

    public function __construct(array $formData) {
        foreach ($formData as $data => $value) {
            $this->$data = $value;
        }
    }

    /**
     * Vérification de la validité d'une donnée eet stockage dans le tableau visitors
     * @param string $key
     * @param AbstractVisitor $visitor
     */
    public function acceptVisitor(string $key, AbstractVisitor $visitor) {
        //visitor = classe chargée du filtrage
        $visit = "Visit" . ucfirst($key);
        if (method_exists($visitor, $visit))
            $this->visitors[$key] = $visitor->visite($this->$key);
    }

    /**
     * Verification du visiteur
     * @return bool
     */
    public function Visit(): bool { //retourne un booléen dans un premier temps, fait le travail de filtre
        foreach ($this->visitors as $data => $bool) {
            if (!$bool) {
                return False;
            }
        }
        return True;
    }

}
