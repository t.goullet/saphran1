<?php

/**
 * Description of CsrfToken
 *
 * @author student
 */
trait CsrfToken {

    private static $token_name = "csrf_token";

    public function session() {
        if (session_id() == "") {
            session_start();
        }
    }

    /**
     * Génération du token
     * @param type $longueur
     * @return string
     */
    public function generateToken($longueur = 20): string {
        $this->session();
        $caracteres = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $longueurMax = strlen($caracteres);
        $chaineAleatoire = '';
        for ($i = 0; $i < $longueur; $i++) {
            $chaineAleatoire .= $caracteres[rand(0, $longueurMax - 1)];
        }
        $_SESSION['csrf_token'] = $chaineAleatoire;
        return $chaineAleatoire;
    }

    /**
     * Verification du token en session si il est identique a celui stocké dans le formulaire
     * @param string $key
     * @return boolean
     */
    public function check(string $key) {
        $this->session();
        $csrf_token = $_SESSION['csrf_token'];
        if (isset($_SESSION['csrf_token'])) {
            if ($key == $csrf_token) {
                return true;
            }
        } else {
            return false;
        }
    }

}
