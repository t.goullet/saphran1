<?php

/**
 * Description of SingletonConfigReader
 *
 * @author student
 */
class SingletonConfigReader {

    private $config;

    /** @var SingletonConfigReader $instance */
    private static $instance = null;
    private static $pathfile;

    private function __construct() {
        self::$pathfile = $_SERVER['DOCUMENT_ROOT'] . '/../config.ini';
        $this->config = parse_ini_file(self::$pathfile, true);
    }

    /**
     * Instancie le SingletonConfigReader
     * @return \SingletonConfigReader
     */
    public static function getInstance(): SingletonConfigReader {
        if (self::$instance == null) {
            self::$instance = new SingletonConfigReader();
        }
        return self::$instance;
    }

    /**
     * Renvoie la valeur associée à la clé dans la section définie
     * @param string $key
     * @param string $section
     * @return string|null
     */
    function getValue(string $key, string $section = null): ?string {
        if ($section != null) {
            return $this->config[$section][$key];
        } else {
            return $this->config[$key];
        }
    }

}
