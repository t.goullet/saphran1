<?php

require_once 'SingletonConfigReader.php';

class SingletonDataBase {
    
    /** @var PDO $cnx*/
    public $cnx;
    private static $serveur;
    private static $database;
    private static $username;
    private static $password;
    
    /** @var SingletonDatabase $instance*/
    public static $instance = null;
    
    
    private function __construct()
    {
      self::$serveur = SingletonConfigReader::getInstance()->getValue('serveur','bdd');
      self::$database = SingletonConfigReader::getInstance()->getValue('database','bdd');
      self::$username = SingletonConfigReader::getInstance()->getValue('username','bdd');
      self::$password = SingletonConfigReader::getInstance()->getValue('password','bdd');
      
      $this->cnx = new PDO('mysql:host='. self::$serveur.';dbname='.self::$database ,self::$username,self::$password);
     
    }
    
    public static function getInstance() : SingletonDataBase
    {
        if (self::$instance == null)
        {
            self::$instance = new SingletonDataBase();
        }
        return self::$instance;
    }
}