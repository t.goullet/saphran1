<?php

/**
 * Description of Renderer
 *
 * @author student
 */
class Renderer {

    /**
     * 
     * @param string $file
     * @param array $data
     * @return type
     */
    public static function render(string $file, array $data = null) {
        $path = "../src" . DIRECTORY_SEPARATOR . "views" . DIRECTORY_SEPARATOR . $file;
        ob_start();
        if ($data != null) {
            extract($data);
        }
        include_once $path;
        $content = ob_get_contents();
        ob_clean();
        return $content;
    }
    
}
