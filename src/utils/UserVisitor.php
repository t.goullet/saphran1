<?php

require_once '../src/utils/AbstractVisitor.php';

/**
 * Description of CityVisitor
 *
 * @author student
 */
class UserVisitor extends AbstractVisitor {

    /** @var DAOCity $daoCity */
    private $daoUser;

    public function __construct() {
        $this->daoUser = new DAOUser(SingletonDataBase::getInstance()->cnx);
    }

    /**
     * Vérifie que le mail de User à bien les pré-requis nécessaires
     * @param string $data
     * @return bool
     */
    public function UserMail(string $data): bool {
        $mailspecial = preg_match('@[@]@', $data);
        $mail = (string) $data;

        if (strlen($data) > 8 && $mail && $mailspecial) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Vérifie que le nom de User à bien les pré-requis nécessaires
     * @param string $data
     * @return bool
     */
    public function UserName(string $data): bool {
        $name = (string) $data;
        if (strlen($name) > 2 && strlen($name) < 60 && ctype_alpha($name)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Vérifie que le prenom de User à bien les pré-requis nécessaires
     * @param string $data
     * @return bool
     */
    public function UserPrenom(string $data): bool {
        $prenom = (string) $data;
        if (strlen($$prenom) > 2 && strlen($prenom) < 60 && ctype_alpha($prenom)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Vérifie que le mots de passe de User à bien les pré-requis nécessaires
     * @param string $data
     * @return bool
     */
    public function UserPassword(string $data): bool {
        $majuscule = preg_match('@[A-Z]@', $data);
        $minuscule = preg_match('@[a-z]@', $data);
        $nombres = preg_match('@[0-9]@', $data);
        $special = preg_match('@[$#?!]@', $data);

        if (strlen($data) > 8 && $majuscule && $minuscule && $nombres && $special) {
            return true;
        } else {
            return false;
        }
    }

}
