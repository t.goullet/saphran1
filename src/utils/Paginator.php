<?php

/**
 * Description of Paginator
 *
 * @author student
 */
trait Paginator {

    /**
     * @param string $url : la base des urls pour generer les liens
     * @param int $page : la page courante (ou l'on se trouve)
     * @param int $total_items
     * @param int $items_per_page : maximum par page : 10 ici 
     */
    public function paginate(string $url, int $page, int $totalItems, int $totalPerPage) {
        $txt = "";
        $hasNext = "enabled";
        $hasPrevious = "enabled";
        $pagePrecedente = $page - 1;
        $pageSuivante = $page + 1;
        $hrefPrevious = $url . "?page=" . $pagePrecedente;
        $hrefNext = $url . "?page=" . $pageSuivante;
        if ($page <= 1) {
            $page = 1;
            $hasPrevious = "disabled";
        } else {
            $txt .= "<li class = 'page-item'><a class = 'page-link' href ='$hrefPrevious'>$pagePrecedente</a></li>";
        }
        $hrefActual = $url . "?page=" . $page;
        $txt .= "<li class = 'page-item'><a class = 'page-link' href ='$hrefActual'>$page</a></li>";
        if ($page >= ceil($totalItems / $totalPerPage)) {
            $hasNext = "disabled";
        } else {
            $txt .= "<li class = 'page-item'><a class = 'page-link' href ='$hrefNext'>$pageSuivante</a></li>";
        }
        $pagination = <<<FIN
                    <ul class = "pagination pagination-sg">
                    <li class = "page-item $hasPrevious"><a class = "page-link" href = "$hrefPrevious">Précédent</a></li>
                    $txt
                    <li class = "page-item $hasNext"><a class = "page-link" href = "$hrefNext">Suivant</a></li>
                    </ul>
                FIN;
        return $pagination;
    }

}
