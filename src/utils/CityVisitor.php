<?php

require_once '../src/utils/AbstractVisitor.php';

/**
 * Description of CityVisitor
 *
 * @author student
 */
class CityVisitor extends AbstractVisitor {

    /** @var DAOCity $daoCity */
    private $daoCity;

    public function __construct() {
        $this->daoCity = new DAOCity(SingletonDataBase::getInstance()->cnx);
    }

    /**
     * Vérifie que le nom de City à bien les pré-requis nécessaires
     * @param string $data
     * @return bool
     */
    public function CityName(string $data): bool {
        $name = (string) $data;
        if (strlen($name) > 2 && strlen($name) < 60 && preg_match('@[A-Z]@', $name)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Vérifie que le District de City à bien les pré-requis nécessaires
     * @param string $data
     * @return bool
     */
    public function CityDistrict(string $data): bool {
        $district = (string) $data;
        if (strlen($region) > 2 && strlen($district) < 100 && preg_match('@[A-Z]@', $district)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Vérifie que la population de City à bien les pré-requis nécessaires
     * @param string $data
     * @return bool
     */
    public function CityPopulation(string $data): bool {
        $population = (int) $data;
        if (strlen($population) <= 5 && preg_match('@[0-9]@', $population)) {
            return true;
        } else {
            return false;
        }
    }

}
