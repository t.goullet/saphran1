/*
 * Insertion des données dans la table roles
 */

insert into roles (Roles_Id, Nom, Permissions) values (1, 'admin', '15');
insert into roles (Roles_Id, Nom, Permissions) values (2, 'client', '14');
insert into roles (Roles_Id, Nom, Permissions) values (3, 'noob', '4');