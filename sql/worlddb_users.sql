
/*
 * Insertion des données dans la table users
 */


insert into users (User_Id, Mail, Password, Nom, Prenom, User_Roles_Id) values (1, 'admin.admin@admin.com', '123+aze', 'admin', 'admin', 1);
insert into users (User_Id, Mail, Password, Nom, Prenom, User_Roles_Id) values (2, 'client.client@client.com', '123+aze', 'client', 'client', 2);
insert into users (User_Id, Mail, Password, Nom, Prenom, User_Roles_Id) values (3, 'noob.noob@noob.com', '123+aze', 'noob', 'noob', 3);
insert into users (User_Id, Mail, Password, Nom, Prenom, User_Roles_Id) values (4, 'cheadon4@stumbleupon.com', 'YWDSRYalC', 'Headon', 'Chic', 2);
insert into users (User_Id, Mail, Password, Nom, Prenom, User_Roles_Id) values (5, 'cfluger5@cdbaby.com', 'Y1KSEC4S', 'Fluger', 'Cazzie', 3);
insert into users (User_Id, Mail, Password, Nom, Prenom, User_Roles_Id) values (6, 'rhelix6@twitpic.com', 'PLdDJam1b0UX', 'Helix', 'Rollin', 2);
insert into users (User_Id, Mail, Password, Nom, Prenom, User_Roles_Id) values (7, 'dparrington7@netscape.com', '3G9zzLqvwe', 'Parrington', 'Dore', 2);
insert into users (User_Id, Mail, Password, Nom, Prenom, User_Roles_Id) values (8, 'bcossar8@reverbnation.com', '86IrCDk', 'Cossar', 'Brucie', 3);
insert into users (User_Id, Mail, Password, Nom, Prenom, User_Roles_Id) values (9, 'whartlebury9@reference.com', '6PHFxAETdaS', 'Hartlebury', 'Whitney', 3);
insert into users (User_Id, Mail, Password, Nom, Prenom, User_Roles_Id) values (10, 'ltebbitta@yahoo.co.jp', 'P8f45D', 'Tebbitt', 'Ludvig', 3);