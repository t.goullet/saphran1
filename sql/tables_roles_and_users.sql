/*
 * Création de la table role
 */

CREATE TABLE roles (
  Roles_Id INT NOT NULL,
  Nom VARCHAR(50),
  Permissions int(2),
  PRIMARY KEY (Roles_Id)
);

/*
 * Création de la table users
 */

CREATE TABLE users (
  User_Id INT NOT NULL,
  Mail VARCHAR(100),
  Password VARCHAR(255),
  Nom VARCHAR(25),
  Prenom VARCHAR (25),
  User_Roles_Id INT NOT NULL,
  PRIMARY KEY (User_Id),
  FOREIGN KEY (User_Roles_Id) REFERENCES roles(Roles_Id)
);